#include <Wire.h>
#include <SPI.h>
#include <XBee.h>
#include <Adafruit_PN532.h>
#include <Adafruit_BMP085.h>

#include <SoftwareSerial.h>

//Pin defines
#define PN532_IRQ   (2)
#define PN532_RESET (3)
#define XBEESLEEP   (9)
#define PIZO        (8)
#define LED_RED     (6)
#define LED_GREEN   (5)
#define BATTERY     (A0)

//Consts defines
#define MAXHEIGHT   (250)

enum FEEDBACK_RESPONSES {
  CHECKIN,
  CHECKOUT,
  UNVALID_CHECKOUT,
  POWER_ON,
  BATTERY_LOW,
  STANDBY,
  ERROR_CODE,
  CARD_DETECTED,
  IS_ON
};

Adafruit_BMP085 bmp;
Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);
XBee xbee = XBee();

double groundAltitude;
double currentAltitude;

uint8_t success;                          // Flag to check if there was an error with the PN532

//For receiving check result of card
Rx16Response rx16 = Rx16Response();
Rx64Response rx64 = Rx64Response();
uint8_t option = 0;
uint8_t data = 0;

bool checkAltitude = true;
bool restart = false;
bool giveStandbyResp = false;
unsigned long timer01;


void setup() {  
  pinMode(PIZO, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(BATTERY,INPUT);
  
  //pin 9 is connected to the xbee pin responsible for sleeping/waking up. HIGH means sleep, LOW means active.
  pinMode(XBEESLEEP, OUTPUT);
  digitalWrite(XBEESLEEP, HIGH);
  
  //Used to test RFID sleep  mode.
  pinMode(PN532_RESET, OUTPUT);
  digitalWrite(PN532_RESET, HIGH);
  
  analogWrite(LED_GREEN, 120);
  digitalWrite(LED_RED, HIGH);
  
  Serial.begin(57600);
  
  ///Use only if bmp is installed on board
  if (!bmp.begin()) { // Could not find a valid BMP085 sensor!
    startupError();   
  }
  
  groundAltitude= bmp.readAltitude(102100);
  
  setupNFC();
  
  xbee.setSerial(Serial); 

  giveFeedback(FEEDBACK_RESPONSES::POWER_ON);
}



String UIDParser(const byte * data, const uint32_t numBytes)
{
  String result = "";
  uint32_t szPos;
  for (szPos = 0; szPos < numBytes; szPos++)
  {
    result += F("0x");
    // Append leading 0 for small values
    if (data[szPos] <= 0xF)
    {
      result += F("0");
    }
    result += String(data[szPos] & 0xff, HEX);
    if ((numBytes > 1) && (szPos != numBytes - 1))
    {
      result += F(" ");

    }
  }
  return result;
}

void loop() {
  //Poll rfid reader
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

  if((millis() - timer01) > 5000)
    checkAltitude=true;
    
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength,2000);
  
  if(checkAltitude=true)
  {
    timer01=millis();
    currentAltitude = bmp.readAltitude(102100);
    checkAltitude=false;
    giveFeedback(FEEDBACK_RESPONSES::IS_ON);  
  }

  
  //If a card is detected
  if (success) {
    giveFeedback(FEEDBACK_RESPONSES::CARD_DETECTED);
    
    // Wake xbee from sleep
    digitalWrite(XBEESLEEP, LOW);
    delay(50);

    String fullKeyUUID = UIDParser(uid, uidLength);
    byte buf[fullKeyUUID.length() + 1];
    fullKeyUUID.toCharArray(buf, fullKeyUUID.length() + 1);

    //Send data to coordinator
    XBeeAddress64 addr64 = XBeeAddress64(0x13A200, 0x4081004D);
    Tx64Request tx = Tx64Request(addr64, buf, sizeof(buf));
    xbee.send(tx);

    //For receiving check response of card
    bool packetRes;
    bool correctPacketRes = false;


    unsigned long time = millis();
    //Checks if correctpacket has been received and if a time out occured (to prevent freezing until a response occurs.
    while (!correctPacketRes && (millis() - time) < 3000)
    {
      packetRes = xbee.readPacket(10000);
      if (packetRes)
      {
        // Add RX_64_RESPONSE to allowed responses? It IS checked later on?
        if (xbee.getResponse().getApiId() == RX_16_RESPONSE)
        {
          correctPacketRes = true;
          
        }
      }
    }
    if(!correctPacketRes)
    {
      giveFeedback(FEEDBACK_RESPONSES::ERROR_CODE);
    }
    if (xbee.getResponse().getApiId() == RX_16_RESPONSE || xbee.getResponse().getApiId() == RX_64_RESPONSE) {

      if (xbee.getResponse().getApiId() == RX_16_RESPONSE) {
        xbee.getResponse().getRx16Response(rx16);
        option = rx16.getOption();
        data = rx16.getData(0);

      }
      else {
        xbee.getResponse().getRx64Response(rx64);
        option = rx64.getOption();
        data = rx64.getData(0);
      }

      //ASCII '1' equals 49 (this is returned if the card is valid and no error occured
      if (data == 49) //success checkin
        giveFeedback(FEEDBACK_RESPONSES::CHECKIN);
      else if(data ==50) //success checkout
        giveFeedback(FEEDBACK_RESPONSES::CHECKOUT);
      else if(data ==51) //unvalid checkout
        giveFeedback(FEEDBACK_RESPONSES::UNVALID_CHECKOUT);
      else //If something else, usually 48, is returned. This means an error of some kind occured. 48 is the standard error return.
        giveFeedback(FEEDBACK_RESPONSES::ERROR_CODE);
    }
    else if (xbee.getResponse().isError()) {

    }


    // Put xbee back to sleep
    digitalWrite(XBEESLEEP, HIGH);

    //Block detecting card until card has been pulled away first.
    while (nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 1000)) {
    }
  }
  
  //For use in sky
  if(currentAltitude - groundAltitude > MAXHEIGHT)
  {
    restart=true;
    if(giveStandbyResp)
    {
      giveFeedback(FEEDBACK_RESPONSES::STANDBY);
    }
    giveStandbyResp=false;
    digitalWrite(PN532_RESET, LOW);
  }
  else
  {
    digitalWrite(PN532_RESET, HIGH);
    if(restart)
    {
      setupNFC();
    }
    restart=false;
    giveStandbyResp=true;
    
  }


  if(analogRead(BATTERY)< 335)
  {
    giveFeedback(FEEDBACK_RESPONSES::BATTERY_LOW);
  }
    
 
}


/*  Feedback in different stages
*/
void giveFeedback(int feedbackEnum)
{
  switch(feedbackEnum)
  {
    case CARD_DETECTED:
      for(int i=0; i <1; i++)
      {
        digitalWrite(LED_GREEN, HIGH);
        delay(500);
        digitalWrite(LED_GREEN, LOW);
        delay(500);
      }
      break;
    case CHECKIN:
      digitalWrite(LED_GREEN, HIGH);
      tone(PIZO, 800, 150);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_GREEN, HIGH);
      delay(200);
      tone(PIZO, 1000, 100);
      digitalWrite(LED_GREEN, LOW);
      break;
    case CHECKOUT:
      digitalWrite(LED_GREEN, HIGH);
      tone(PIZO, 1000, 150);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_GREEN, HIGH);
      delay(200);
      tone(PIZO, 800, 100);
      digitalWrite(LED_GREEN, LOW);
      break;
    case UNVALID_CHECKOUT:
      for(int i=0; i <3; i++)
      {
        digitalWrite(LED_RED, HIGH);
        tone(PIZO, 500, 500);
        delay(500);
        digitalWrite(LED_RED, LOW);
        delay(500);
      }
      break;
    case POWER_ON:
        digitalWrite(LED_GREEN,HIGH);
        digitalWrite(LED_RED,HIGH);
        tone(PIZO, 500, 300);
        delay(350);
        tone(PIZO, 600,300);
        delay(350);
        tone(PIZO, 400,300);
        delay(300);
        tone(PIZO, 500,300);
        delay(300);
        tone(PIZO, 400,500);
        delay(500);
        digitalWrite(LED_GREEN,LOW);
        digitalWrite(LED_RED,LOW);
        break;
    case BATTERY_LOW:
        digitalWrite(LED_RED, HIGH);
        tone(PIZO, 500, 250);
        delay(250);
        tone(PIZO, 400, 250);
        delay(250);
        tone(PIZO, 300, 250);
        digitalWrite(LED_RED,LOW);    
      break;
    case STANDBY:
      digitalWrite(LED_GREEN, HIGH);
      tone(PIZO, 500, 250);
      delay(250);
      tone(PIZO, 400, 250); 
      digitalWrite(LED_GREEN, LOW); 
      break;
    case IS_ON:
      digitalWrite(LED_GREEN, HIGH);
      delay(100);
      digitalWrite(LED_GREEN, LOW);
      digitalWrite(LED_RED, HIGH);
      delay(100);
      digitalWrite(LED_RED, LOW); 
      break;
    case ERROR_CODE:
      for(int i=0; i <4; i++)
      {
        digitalWrite(LED_RED, HIGH);
        tone(PIZO, 500, 500);
        delay(500);
        digitalWrite(LED_RED, LOW);
        delay(500);       
      }
      break;
  }  
}


/* Init the NFC reader with correct config. This needs to be done after each reset
*/
void setupNFC()
{
  nfc.begin();
  //Amount of retries allowed when reading a card, is used to detect if the card has been pulled away from the reader
  nfc.setPassiveActivationRetries(0xFF);
  uint32_t versiondata = nfc.getFirmwareVersion();
  
  if (! versiondata) {       // Could not find a PN53x board!
    startupError();
  }
  nfc.SAMConfig();    // Configure board to read RFID tags 
}


/*  When there has an error occured while startup or init
*   this function will be called, so the user knows
*   something is wrong
*/
void startupError(){
  digitalWrite(LED_GREEN, LOW);
  while(1){
    digitalWrite(LED_RED, HIGH);
    tone(PIZO, 500, 100);    
    delay(1000);
    digitalWrite(LED_RED, LOW);
    tone(PIZO, 400, 100);     
    delay(1000);  
  }
}


